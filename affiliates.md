# Affiliates

The affiliates resource endpoints allow details to be retrieved, new affiliates/pharmacies to be created and existing ones to be updated.

Data will be used in the generation of the checkout template using the logo, address and styling. It is important to ensure logo urls are set for all pharmacies on a https url, otherwise the checkout page will show an error to the customer. Address details will be added to our footer template.
In order to enable customisation we have created a `<style>` block in the template header to enable you to make the checkout pages fit the pharmacy branding. Sensible defaults will be used when there are no settings defined. A sample page can be viewed at [https://affiliates.weldricks.co.uk/api/v1/checkout/sample/{id}] where id is the id of the affiliate/pharmacy.

## Request Properties

- **id:** (int) The pharmacy id.
- **url:** (string) URL of the pharmacy homepage.
- **return_url:** (string) URL to redirect the customer to on completed checkout.
- **email:** (string) Validated email address for the pharmacy.
- **name:** (string) Name of the pharmacy.
- **logo:** (string) URL of the logo (HTTPS).
- **address:** Street address.
- **town:** (string) Town.
- **postcode:** (string) Postcode.
- **country:** (string) Two digit country code (GB etc).
- **styles:** (string) Here you can include css styles to be injected into a <style> block in the template head.
- **checkout:** (boolean) This is a flag if the pharmacy is allowed to checkout.
- **admin:** (boolean) A flag to allow this account to ADD/UPDATE (see below).
- **created_at:** (date) Date the pharmacy was created.
- **updated_at:** (date) Date the pharmacy was last updated.

- **password:** (string, min 8 characters) The auth password (hidden in GET requests, required on POST, optional on PUT).

## Get Affiliate Information

When calling an affiliate specific url e.g. [https://affiliates.weldricks.co.uk/api/v1/affiliates/{id}] where id is the id of the affiliate/pharmacy a json representation is returned with data as detailed above, auth credentials must match the id requested.

### Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/api/v1/affiliates/123', [
    'auth' => ['id', 'pass']
]);
```

### Example Response

```json
{
    "id": 123,
    "url": "https://www.examplepharmacy.co.uk",
    "return_url": "https://www.examplepharmacy.co.uk/order-complete",
    "email": "info@examplepharmacy.co.uk",
    "name": "Example Pharmacy",
    "logo": "https://www.examplepharmacy.co.uk/logo.png",
    "address": "123 A Street",
    "town": "Doncaster",
    "postcode": "DN1 1AA",
    "country": "GB",
    "styles": "body{color:#000} h1{color:#f00}",
    "checkout": 1,
    "admin": 0,
    "created_at": "2016-05-18 10:36:01",
    "updated_at": "2016-05-18 10:36:01"
}
```

## Create A New Affiliate

When creating a new affiliate/pharmacy a json representation with the properties detailed above should be sent as a POST request to the affiliates endpoint [https://affiliates.weldricks.co.uk/api/v1/affiliates], auth credentials should be for an affiliate authorised for affiliate management (contact webmaster@weldricks.co.uk)

If the request is successful a HTTP 200 status response will return a json affiliate object as detailed above. If validation fails a HTTP 422 status response will return a json object with keys representing fields with errors and values being an array of string messages for display.

### Example Request

```php
$response = $client->request('POST', 'https://affiliates.weldricks.co.uk/api/v1/affiliates', [
    'auth' => ['id', 'pass'],
    'json' => [
        'password' => 'abcdefg12345678',
        'name' => 'Example Pharmacy',
        'email' => 'info@examplepharmacy.co.uk',
        'url' => 'https://www.examplepharmacy.co.uk',
        'logo' => 'https://www.examplepharmacy.co.uk/logo.png',
        'address' => '123 A Street',
        'town' => 'Doncaster',
        'postcode' => 'DN1 1AA',
        'country' => 'GB',
        'styles' => 'body{color:#000} h1{color:#f00}',
        'return_url' => 'https://www.examplepharmacy.co.uk/order-complete',
    ]
]);
```

## Update An Affiliate

In order to update the details of an existing affiliate/pharmacy one or more of the properties detailed above should be sent as a HTTP PUT request to the [https://affiliates.weldricks.co.uk/api/v1/affiliates/{id}] endpoint. Auth credentials should be for an affiliate authorised for affiliate management or the affiliate being updated.

### Example Request

The example below specifies a new logo and border radius for the affiliate, all other properties will remain unchanged.

```php
$response = $client->request('PUT', 'https://affiliates.weldricks.co.uk/api/v1/affiliates/123', [
    'auth' => ['id', 'pass'],
    'json' => [
        'logo' => 'https://www.examplepharmacy.co.uk/logo2.png',
        "styles": "body{color:#000} h1{color:#f00}"
    ]
]);
```
