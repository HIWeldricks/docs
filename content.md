# Site Content

Some content is supplied by Weldricks Pharmacy to use on affiliate sites. We currently list 'Terms and Conditions' and 'Delivery'


## Terms and Conditions

The terms and conditions given by Weldricks Pharmacy are available to add to the affiliate site or appended to any existing terms of the affiliate site.

### Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/api/v1/terms', [
    'auth' => ['id', 'pass']
]);
```

### Example Response

The response should be plain HTML.


## Delivery

The delivery information for Weldricks Pharmacy is available to add to the affiliate site.

### Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/api/v1/delivery', [
    'auth' => ['id', 'pass']
]);
```

### Example Response

The response should be plain HTML.

[Back](readme.md)
