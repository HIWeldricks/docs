# Weldricks API Documentation

## 10th May update please read

[Affiliates](affiliates.md)

## Examples

Request examples are provided using the latest [Guzzle PHP library](http://docs.guzzlephp.org/en/latest/index.html):

```php
use GuzzleHttp\Client;

$client = new Client();
```

## Security

The API implements HTTP basic authentication, username is the pharmacy id.

```php
$client->request('GET', 'https://affiliates.weldricks.co.uk/api/v1/products', [
    'auth' => ['id', 'pass']
]);
```

## Contents

1. [Categories](categories.md)
2. [Products](products.md)
3. [Basket](basket.md)
4. [Checkout](checkout.md)
5. [Orders](orders.md)
6. [Webhooks](webhooks.md)
7. [Site Content](content.md)
8. [Affiliates](affiliates.md)
9. [Affiliates post Payment](affiliatespostpayment.md)
