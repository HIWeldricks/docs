# Products

A product feed is available to populate your database at [https://affiliates.weldricks.co.uk/api/v1/products].
200 products are listed per page, ordered by the most recently updated first, subsequent pages can be accessed by appending
a page variable to the request e.g. [https://affiliates.weldricks.co.uk/api/v1/products?page=2].

The response includes a data property, an array of products, each having properties:

- **id:** (int) The unique id assigned to the product
- **name:** (string 100) The full name of the product
- **price:** (decimal 8,2) Current price for the item
- **rrp:** (decimal 8,2) Recommended Retail Price for product
- **vat:** (int) VAT rate applied to this product
- **commission:** (int) Rate of commission paid for sale of this product
- **image:** Link to source image for you to download resize to your own requirements and host on your own server
- **type:** (string 3) the type of product listed (N = No type | GSL = General sales line | P = Pharmacist | POM = Prescription only medication | VET = Vet prescription)
- **warehouse:** (boolean) 1 = in warehouse, 0 = needs to be ordered
- **active:** (boolean) If the product is listed on the site.
- **availability:** (string 25) The availability status of the product (InStock = In stock and available for sale | OutOfStock = out of stock and cannot be bought)
- **updated_at:** (timestamp) The date the product data was last updated
- **links:** An array of related links
    - **uri** contains address for single product information
- **tabs:** The nested array for product information, includes:
    - **id** (int) The id of the tab
    - **name** (string 20) The name of the tab
    - **html** The html content of the tab
- **categories:** The nested array of [categories](#categories) the product is currently listed in

The response also includes a meta property with a pagination object defining:

- **total:** The total number of products
- **count:** The number in the current page,
- **per_page:** The number of products returned per page,
- **current_page:** The current page number
- **total_pages:** The total number of pages
- **links:** An object containing relative links as appropriate
    - **next:** The url of the next page in the sequence (*not included on the last page*)
    - **prev:** The url of the prev page in the sequence (*not included on the first page*)

## Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/api/v1/products', [
    'auth' => ['id', 'pass']
]);
```

## Example Response

```json
{
  "data": [
    {
      "id": 1931,
      "name": "Elgydium Toothpaste Anti-plaque 75ml",
      "price": "2.79",
      "rrp": "3.50",
      "image": null,
      "type": "N",
      "availability": "OutOfStock",
      "updated_at": "2016-04-13 15:48:17",
      "links": [
        {
          "rel": "self",
          "uri": "https://affiliates.weldricks.co.uk/api/v1/products/1931"
        }
      ],
      "tabs": {
        "data": [
          {
            "id": 6917,
            "name": "Directions",
            "html": "<p>Learning how to brush your teeth properly is the first step to maintaining healthy teeth and gums. Plus, it helps minimise the risk of tooth decay and gum disease, the major causes of tooth loss.</p> <h3>Before You Begin</h3> <p>While there are several methods of brushing teeth with a manual toothbrush, always ask your dental professional for their recommendation and be sure to follow their instructions. To start, use fluoride toothpaste with a soft-bristle toothbrush, and don&rsquo;t forget to replace it every three months.</p> <h3>Two Minutes, Twice a Day</h3> <p>To brush your teeth correctly, spend at least two minutes using a recommended technique, which includes 30 seconds brushing each section of your mouth (upper right, upper left, lower right and lower left), both morning and night. Since most manual toothbrushes don&rsquo;t have built-in two-minute timers, you may want to have a clock handy so you can be sure you&rsquo;re brushing long enough.</p> <h3>Positioning the Toothbrush</h3> <p><strong>How you hold the toothbrush depends on which part of the tooth you&rsquo;re brushing.</strong></p> <p> <ul> <li>Step 1: Start with outer and inner surfaces, and brush at a 45-degree angle in short, half-tooth-wide strokes against the gum line. Make sure you reach your back teeth.</li> <li>Step 2: Move on to chewing surfaces. Hold the brush flat and brush back and forth along these surfaces.</li> <li>Step 3: Once you get to the inside surfaces of your front teeth, tilt the brush vertically and use gentle up-and-down strokes with the tip of brush.</li> <li>Step 4: Be sure to brush gently along the gum line.</li> <li>Step 5: Brush your tongue in a back-to-front sweeping motion to remove food particles and help remove odour-causing bacteria to freshen your breath.</li> <li>Step 6: Try gently brushing the roof of your mouth for an extra-fresh feeling.</li> </ul> </p> <p>Now that you&rsquo;ve learned proper brushing technique, a little discipline in practicing it every day will help make it feel like second nature. It&rsquo;s one of the easiest things you can do to maintain the health of your teeth and gums.</p>",
            "updated_at": "-0001-11-30 00:00:00"
          },
          {
            "id": 6918,
            "name": "Warnings",
            "html": "<p>Do not swallow.</p> <p>In case of intake of fluoride from other sources consult a dentist or doctor.&nbsp;</p> <p>Sensitive teeth may indicate an underlying problem which needs prompt care by a dentist. See your dentist as soon as possible for advice.</p> <div><br /></div>",
            "updated_at": "-0001-11-30 00:00:00"
          },
          {
            "id": 6919,
            "name": "Ingredients",
            "html": "<p>Aque, calcium carbonate, glycerin, hydrated silica, sodium lauryl sulfate, aroma (flavour), cvhondrus crispus (carrageenan), Benzyl alcohol, cellulose gum, chlorhexidine digluconate, limonene, methylparaben, propylparaben, sodium saccharin, CI 77891 (titanium dioxide).</p>",
            "updated_at": "-0001-11-30 00:00:00"
          }
        ]
      },
      "categories": {
        "data": [
          {
            "id": 1138,
            "parent_id": 223,
            "menu": "Toothpaste Protection",
            "title": "Toothpaste Protection",
            "href": "/essentials/dental-care/toothpaste-protection",
            "updated_at": "2016-02-26 13:27:44",
            "links": [
              {
                "rel": "self",
                "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/1138"
              }
            ]
          },
          {
            "id": 1319,
            "parent_id": 384,
            "menu": "Offers",
            "title": "Offers",
            "href": "/offers/february-offers/offers",
            "updated_at": "2016-02-26 13:27:45",
            "links": [
              {
                "rel": "self",
                "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/1319"
              }
            ]
          }
        ]
      }
    }
    ...
  ],
  "meta": {
      "pagination": {
          "total": 12730,
          "count": 200,
          "per_page": 200,
          "current_page": 1,
          "total_pages": 64,
          "links": {
            "next": "https://affiliates.weldricks.co.uk/api/v1/products?page=2"
          }
      }
  }
}
```

# Product

When calling a product specific url e.g. [https://affiliates.weldricks.co.uk/api/v1/products/{id}] where id is the id of the product
called, the single resource is returned with data as detailed above.

[Back](readme.md)
