# Webhooks

The [category](categories.md), [product](products.md) and [order](orders.md) api endpoints provide data for all resources that are available
at the time they are requested. Webhooks provide the functionality for our servers to send updates directly to yours via
HTTP POST requests as soon as resources are added/updated/deleted, this should help to ensure your site visitors always see the
most up to date content. Webhooks are also the method we will use to pass updated information on orders.

## Setup

In order to setup your webhook you should email [mlaw@weldricks.co.uk](mailto:mlaw@weldricks.co.uk) and include the url your servers will receive our POST
requests on. You should ensure the url is SSL encrypted and may also include username and password for HTTP Basic Authentication.

## Requests

Whenever a resource is added/updated/deleted on our servers the data will be submitted as a POST request to your webhook
url containing a JSON encoded object with properties:

- **resource:** The type of resource being submitted (product|category|order)
- **id:** The id of the resource
- **data:** The resource object (see below)

## Resource Data

For added or updated resources the data object will include properties specified in the [category](categories.md),
[product](products.md) and [order](orders.md) feeds.

In the case of resources deleted or no longer available the data object will be empty and upon identifying the corresponding
record in your systems you should ensure it is no longer available to your site visitors.

**NB:** You may receive requests with data that does not appear to have been updated or empty data for
resources with ids you do not have stored, this will occur as all updates on our systems trigger the webhooks but this may
not be relevant to the data you hold (e.g. adding internal admin notes, or preparing new products before release) in those
cases the requests can be ignored.

[Back](readme.md)
