# Checkout post Payment

Once a customer has made a payment the URL https://affiliates.weldricks.co.uk/checkout/complete will redirect to the given redirect_url in the appropriate pharmacy record (see [Affiliates](affiliates.md))

## Returned Properties
- **order_id:** Weldrick's Order ID which is passed back on a successful Checkout ([Checkout](checkout.md))
- **status:** Returns either OK or ERROR
- **reason:** If 'status' is ERROR this will contain the reason for the error
- **order_status:** The new status for the order to update your records. on 'status' OK this will be 'pending', on 'ERROR' it will be 'created'
- **type:** Payment type of the order; Sagepay or PayPal

## Response Properties

We do not require a response.

[Back](readme.md)
