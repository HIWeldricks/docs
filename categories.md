# Categories

A category feed is available to populate your database at [https://affiliates.weldricks.co.uk/api/v1/categories]

The response includes a data property which is a nested array of categories, each having properites:

- **id:** (int) The unique id assigned to the category
- **parent_id:** (int) The id of the parent category in the nested structure (null for top level)
- **menu:** (string 50) The category name as it appears in the site navigation menu
- **title:** (string 100) The full title of the category
- **updated_at** (timestamp) The date & time the category was last updated
- **links:** An array of links to related data
    - **uri** contains address for more detailed information
- **categories:** The nested array of child categories

## Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/api/v1/categories', [
   'auth' => ['id', 'pass']
]);
```

## Example Response
```json
{
  "data": [
    {
      "id": 102,
      "parent_id": null,
      "menu": "Embarrassing",
      "title": "Embarrassing",
      "href": "/embarrassing",
      "updated_at": "2016-02-26 13:27:39",
      "links": [
        {
          "rel": "self",
          "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/102"
        }
      ],
      "categories": {
        "data": [
          {
            "id": 203,
            "parent_id": 102,
            "menu": "Acne",
            "title": "Acne",
            "href": "/embarrassing/acne",
            "updated_at": "2016-02-26 13:27:39",
            "links": [
              {
                "rel": "self",
                "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/203"
              }
            ],
            "categories": {
              "data": [
                {
                  "id": 501,
                  "parent_id": 203,
                  "menu": "Treatments",
                  "title": "Treatments",
                  "href": "/embarrassing/acne/treatments",
                  "updated_at": "2016-02-26 13:27:41",
                  "links": [
                    {
                      "rel": "self",
                      "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/501"
                    }
                  ]
                },
                {
                  "id": 959,
                  "parent_id": 203,
                  "menu": "Gels, Creams & Lotions",
                  "title": "Gels, Creams & Lotions",
                  "href": "/embarrassing/acne/gels-creams-and-lotions",
                  "updated_at": "2016-02-26 13:27:43",
                  "links": [
                    {
                      "rel": "self",
                      "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/959"
                    }
                  ]
                },
                ...
              ]
            }
          },
          {
            "id": 204,
            "parent_id": 102,
            "menu": "Bad Breath",
            "title": "Bad Breath",
            "href": "/embarrassing/bad-breath",
            "updated_at": "2016-02-26 13:27:39",
            "links": [
              {
                "rel": "self",
                "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/204"
              }
            ],
            "categories": {
              "data": [
                {
                  "id": 641,
                  "parent_id": 204,
                  "menu": "Mouthwashes",
                  "title": "Mouthwashes",
                  "href": "/embarrassing/bad-breath/mouthwashes",
                  "updated_at": "2016-02-26 13:27:41",
                  "links": [
                    {
                      "rel": "self",
                      "uri": "https://affiliates.weldricks.co.uk/api/v1/categories/641"
                    }
                  ]
                },
                ...
              ]
            }
          }
        ]
      ...
    ...
```
# Cateogory

When calling a category specific url e.g. [https://affiliates.weldricks.co.uk/api/v1/categories/{id}] where id is the id of the category
called, an additional array of product objects will be added to the data, this will represent the products linked to the category.
The inverse is available from the [product resource](products.md).

[Back](readme.md)
