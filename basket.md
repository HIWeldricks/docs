# Basket

The basket api endpoint [https://affiliates.weldricks.co.uk/api/v1/basket] accepts an HTTP POST request with a json representation
of your basket then returns the basket with invalid products removed and additional computed data such as error messages,
total costs and delivery options/costs.

## Request Properties
- **products:** An array of products in the basket
    - **id:** = (int) The product id
    - **name:** = (string 100) The name of the product
    - **quantity:** = (int 11) The quantity selected
- **deliveryType:** (int) The selected delivery option **may be null when building basket**

## Response Properties
- **errors:** An array of strings reporting errors with the submitted basket
- **products:** An array of products in the basket:
    - **id:** (int) The product id
    - **name:** (string 100) The name of the product
    - **quantity:** (int 11) The quantity selected
    - **price:** (decimal 8,2) The current price
    - **vat:** (integer) The vate rate for this product
    - **subTotal:** (decimal 8,2)The sub-total (price * quantity)
- **items:** (int) The total number of items in the basket
- **vat:** (decimal 8,2) The total VAT for order
- **subTotal:** (decimal 8,2) The sub-total of items
- **total:** (decimal 8,2) The total basket cost (inc. delivery)
- **deliveryOptions:** An array of delivery options grouped by region:
    - **region:** (string 100) The global region for the delivery (United Kingdom|Europe|International)
    - **options:** The options for the region, may contain a single disabled option if delivery is not available in the region:
        - **id:** (int) The delivery option id (to be used as selected delivery type)
        - **name:** (string 15) The delivery option name
        - **price:** (decimal 8,2) The price of this delivery option
        - **disabled:** Boolean to indicate whether the option is available or disabled
- **deliveryType:** (int) The selected delivery type (defaults to first available)
- **delivery:** (decimal 8,2) The cost of delivery

## Example Request

In the examples below note the quantity of 4 on the Solpadeine product in the request, then the response with
an error warning of a limit on Paracetamol products and the quantity reduced to 3.

```php
$response = $client->request('POST', 'https://affiliates.weldricks.co.uk/api/v1/basket', [
    'auth' => ['id', 'pass'],
    'json' => [
        'products' => [
            [
                'id' => 4265,
                'name' => 'Solpadeine Max Soluble Pack of 32',
                'quantity' => 4,
            ],
            [
                'id' => 5204,
                'name' => 'Malibu Continuous Dry Oil Spray SPF15 175ml',
                'quantity' => 1,
            ],
        ],
        'deliveryType' => null,
    ]
]);
```

## Example Response

```json
{
  "errors": [
    "Solpadeine Max Soluble Pack of 32: Paracetamol products are limited to a total of 100 tablets\/capsules per order."
  ],
  "products": [
    {
      "id": 4265,
      "name": "Solpadeine Max Soluble Pack of 32",
      "quantity": 3,
      "subTotal": 16.47,
      "price": 5.49
    },
    {
      "id": 5204,
      "name": "Malibu Continuous Dry Oil Spray SPF15 175ml",
      "quantity": 1,
      "subTotal": 2.79,
      "price": 2.79
    }
  ],
  "items": 4,
  "vat": 3.21,
  "subTotal": 19.26,
  "total": 22.45,
  "deliveryOptions": [
    {
      "region": "United Kingdom",
      "options": [
        {
          "id": 1,
          "name": "Standard",
          "price": 3.19,
          "disabled": false
        },
        {
          "id": 2,
          "name": "Tracked and Signed",
          "price": 3.49,
          "disabled": false
        },
        {
          "id": 3,
          "name": "Next Day",
          "price": 5.48,
          "disabled": false
        }
      ]
    },
    {
      "region": "Europe\/EU",
      "options": [
        {
          "id": 0,
          "name": "Not available with selected products",
          "price": null,
          "disabled": true
        }
      ]
    },
    {
      "region": "International",
      "options": [
        {
          "id": 0,
          "name": "Not available with selected products",
          "price": null,
          "disabled": true
        }
      ]
    }
  ],
  "deliveryType": "1",
  "delivery": 3.19
}
```


[Back](readme.md)
