# Orders

An orders feed is available to populate your database at [https://affiliates.weldricks.co.uk/api/v1/orders].
The orders returned will be restricted to those originating from the affiliate login credentials.
200 orders are listed per page, ordered by the most recently updated first, subsequent pages can be accessed by appending
a page variable to the request e.g. [https://affiliates.weldricks.co.uk/api/v1/orders?page=2].

The response includes a data property, an array of orders, each having properties:

- **id:** (int) The unique id assigned to the order,
- **affiliate_id** (int) The unique id of the affiliate
- **txcode**  - (string) The unique order number
- **status** - (string) Current status of the order (pending, processing, dispatched or aborted)
- **status_date** - (date) The date the status was changed last
- **awaiting_authorisation** - (boolean) If the order contains medicines that require a Pharmacist to authorise (1 = waiting)
- **awaiting_doctor** - (boolean) If the order contains Online Doctor products that require a Doctor to authorise (1 = waiting)
- **on_hold** - (boolean) Set to 1 if the order has been put on hold (see held_reason)
- **delayed** - (boolean) Set to 1 is the order has been delayed (see delayed_reason)
- **processed_at** - (date) Date the order began being processed
- **held_reason** - (string) The reason why the order has been put on hold
- **delayed_reason** - (string) The reason why the order has been delayed
- **dispatched_at** - (date) The date the order was dispatched
- **aborted_at** - (date) The date the order was aborted
- **sub_total** - (decimal 8,2) Sub total the the order
- **delivery** - (decimal 8,2) The amount paid for delivery
- **discount** - (decimal 8,2) Amount applied in discounts
- **vat** (decimal 8,2) VAT total for the order
- **total** (decimal 8,2) Total amount paid for the order
- **delivery_type** - (int) Number relating to the delivery type (see below)
- **tracking_code** - (string) Tracking number used from Royal Mail
- **affiliate_order_id** - (int) Order ID supplied from affiliate site.
- **updated_at** - (date) the date order was last updated

The response also includes a meta property with a pagination object defining:

- **total:** The total number of orders
- **count:** The number in the current page,
- **per_page:** The number of orders returned per page,
- **current_page:** The current page number
- **total_pages:** The total number of pages
- **links:** An object containing relative links as appropriate
    - **next:** The url of the next page in the sequence (*not included on the last page*)
    - **prev:** The url of the prev page in the sequence (*not included on the first page*)

#### Delivery types
1. Standard
2. Tracked and Signed
3. Next Day
4. Click and Collect
5. Europe/EU
6. International

## Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/api/v1/orders', [
    'auth' => ['id', 'pass']
]);
```


# Order

When calling an order specific url e.g. [https://affiliates.weldricks.co.uk/api/v1/orders/{id}] where id is the id of the order
called, the single resource is returned with data as detailed above. Please note: the login credentials must match the affiliate
that generated the order.


# Webhook
An order object will be supplied via webhook on update of affiliate orders. The resource will be 'order'. see [Webhooks](webhooks.md)


[Back](readme.md)
